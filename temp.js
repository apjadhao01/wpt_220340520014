const express = require("express");
const app = express();
//const cors = require('cors');
//app.use(cors());

//const bodyParser = require('body-parser');

//JSON.stringify(result)

app.use(express.static("abc"));
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: true }));

const mysql = require("mysql2");

let databaseparams = {
  host: "localhost",
  user: "root",
  password: "cdac",
  database: "mumbai",
  port: 3306,
};

const con = mysql.createConnection(databaseparams);


// Blur Event On BookID  /getbook

app.get("/getbook", function (req, res) {
  let bookid = req.query.x;
  console.log(bookid);

  let output = { status: false, details: { bookname: " ", price: " " } };

  console.log("reading input " + bookid);

  con.query("select * from book where bookid = ?", [bookid], (err, rows) => {
    if (err) {
      console.log(err);
    } else {
      if (rows.length > 0) {
        output.status = true;
        output.details.bookname = rows[0].bookname;
        output.details.price = rows[0].price;

        console.log(output.details.price);
      }
    }

    res.send(output);
  });
});


// Blur Event on Book Name  /getbookname

app.get("/getbookname", function (req, res) {
  let bookname = req.query.x;
  console.log(bookname);

  let output = { status: false, details: { bookid: " ", price: " " } };

  console.log("reading input " + bookname);

  con.query(
    "select * from book where bookname = ?",
    [bookname],
    (err, rows) => {
      if (err) {
        console.log(err);
      } else {
        if (rows.length > 0) {
          output.status = true;
          output.details.bookid = rows[0].bookid;
          output.details.price = rows[0].price;

          console.log(output.details.price);
        }
      }

      res.send(output);
    }
  );
});


// Click event on Update

app.get("/update", (req, resp) => {
  let bookid = req.query.a;
  let price = req.query.b;

  console.log(bookid + " " + price);

  let output = false;

  con.query(
    'update book set price = ? where bookid = ?',
    [price, bookid],
    (err, rows) => {
      if (err) {
      } else {
        if (rows.affectedRows > 0) {
          output = true;
        }
      }

      resp.send(output);
    }
  );
});



app.listen(8081, function () {
  console.log("server listening at port 8081...");
});
